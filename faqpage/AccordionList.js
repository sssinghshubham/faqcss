import React, { useState } from "react";
import SubitemList from "./SubItemList";

import classes from "./AccordionList.module.css";
const AccordionList = ({ items }) => {
  const [activeIndex, setActiveIndex] = useState(null);

  const toggleItem = (index) => {
    setActiveIndex(index === activeIndex ? null : index);
  };

  return (
    <ul className={classes["accordion-list"]}>
      {items.map((item, index) => (
        <li key={index} className={classes["accordion-item"]}>
          <div
            className={`${classes["accordion-header"]}${
              index === activeIndex ? "active" : ""
            }`}
            onClick={() => toggleItem(index)}
          >
            {item.title}
          </div>
          {index === activeIndex && (
            <ul className={classes.sublist}>
              {item.subitems.map((subitem, subIndex) => (
                <SubitemList
                  key={subIndex}
                  data={subitem}
                  keyIndex={subIndex}
                />
              ))}
            </ul>
          )}
        </li>
      ))}
    </ul>
  );
};
export default AccordionList;
