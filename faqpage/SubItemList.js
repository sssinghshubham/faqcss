import React, { useState } from "react";
import classes from "./SubItemList.module.css";
const SubItemList = ({ data, keyIndex }) => {
  const [activeIndex, setActiveIndex] = useState(null);

  const toggleItem = (index) => {
    setActiveIndex(index === activeIndex ? null : index);
  };

  return (
    <ul className={classes.accordians}>
      {console.log(data)}

      <li className={classes.accordion_item}>
        <div
          className={classes.accordian_item_div}
          onClick={() => toggleItem(keyIndex)}
        >
          {data.title}
        </div>
        {keyIndex === activeIndex && (
          <ul className={classes.sublist}>
            {data.answer.map((subitem, subIndex) => (
              <li key={subIndex}>{subitem}</li>
            ))}
          </ul>
        )}
      </li>
    </ul>
  );
};
export default SubItemList;
